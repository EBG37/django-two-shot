from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseForm, AccountForm

# Create your views here.


@login_required
def show_receipt(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipts
    }
    return render(request, "receipts/list.html", context)


@login_required()
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            form.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def show_expenses(request):
    expenses = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "expenses": expenses
    }
    return render(request, "receipts/expense.html", context)


@login_required
def show_accounts(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def create_expense(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            form.save()
            return redirect("category_list")
    else:
        form = ExpenseForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/createexpense.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            form.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/createaccount.html", context)