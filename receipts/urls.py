from django.urls import path
from receipts.views import show_receipt, create_receipt, show_expenses
from receipts.views import show_accounts, create_expense, create_account
urlpatterns = [
    path("accounts/create/", create_account, name="create_account"),
    path("categories/create/", create_expense, name="create_category"),
    path("accounts/", show_accounts, name="account_list"),
    path("categories/", show_expenses, name="category_list"),
    path("create/", create_receipt, name="create_receipt"),
    path("", show_receipt, name="home"),
]